var $$ = Dom7;

var app = new Framework7({
  root: '#app', // App root element

  id: 'be.olislaegersmatthieuodisee.mySandwich', // App bundle ID
  name: 'mySandwich', // App name
  theme: 'auto', // Automatic theme detection

  // App root data
  data: function () {
    return {
      user: {
        firstName: 'John',
        lastName: 'Doe',
      },
      // Demo products for Catalog section
      products: [{
          id: '1',
          title: 'Apple iPhone 8',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
        },
        {
          id: '2',
          title: 'Apple iPhone 8 Plus',
          description: 'Velit odit autem modi saepe ratione totam minus, aperiam, labore quia provident temporibus quasi est ut aliquid blanditiis beatae suscipit odio vel! Nostrum porro sunt sint eveniet maiores, dolorem itaque!'
        },
        {
          id: '3',
          title: 'Apple iPhone X',
          description: 'Expedita sequi perferendis quod illum pariatur aliquam, alias laboriosam! Vero blanditiis placeat, mollitia necessitatibus reprehenderit. Labore dolores amet quos, accusamus earum asperiores officiis assumenda optio architecto quia neque, quae eum.'
        },
      ]
    };
  },
  // App root methods
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
  },
  // App routes
  routes: routes,


  // Input settings
  input: {
    scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
    scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron,
  },
  // Cordova Statusbar settings
  statusbar: {
    iosOverlaysWebView: true,
    androidOverlaysWebView: false,
  },


  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);
      }
    },
    pageInit: function (page) {
      // andere methode om functies aan een knop te hangen
      if (page.route.name === "home") {
        getListUsers();
      }
      if (page.route.name === "admin-delete") {
        getListAdmin();
      }
      if (page.route.name === "admin-orders") {
        getAllOrders();
      }
      if (page.route.name === "admin-add") {
        $$('#btnVoegToe').on('click', function () {
          AddSandwich();
        });
      }
      if (page.route.name === "login") {
        $$('#btnLogin').on('click', function () {
          LoginUser();

        });
      }
      if (page.route.name === "register") {
        $$("#btnRegister").on('click', function () {
          registerUser();
        });
      }
    }

  },
});


var apiAddress = "https://myhostingname.be/";
var opties = {
  method: "POST", // *GET, POST, PUT, DELETE, etc.
  mode: "cors", // no-cors, *cors, same-origin
  cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
  credentials: "omit", // include, *same-origin, omit
};

var notificationFull = app.notification.create({
  icon: '<i class="icon demo-icon">7</i>',
  title: 'mySandwich',
  subtitle: 'Your order is ready!',
  text: '(Tap to close)',
  closeOnClick: true,
});

function getListAdmin() {

  let url = apiAddress + "php/producten.php?";

  opties.body = JSON.stringify({
    format: "json",
    table: "sandwiches",
    bewerking: "get"
  });

  // fetch api
  fetch(url, opties)
    .then(function (response) {
      return response.json();
    })
    // als het lukt
    .then(function (responseData) {

      // de data
      var list = responseData.data;

      // er zit minstens 1 item in list.
      if (list.length > 0) {

        let card = "";
        for (let i = 0, len = list.length; i < len; i++) {
          card += `<div class="card card-outline">` +
            ` <div class="card-header">${list[i].name}</div>` +
            ` <div class="card-content card-content-padding">` +
            ` <div class='block block-strong inset'>` +
            ` <p>Extra: ${list[i].extra}</p>` +
            ` <p>Kcal: ${list[i].kcal} </p>` +
            ` <p>Saus: ${list[i].saus} </p>` +
            ` <p>Prijs:€ ${list[i].price}.00 </p> </div>` +
            `<div class="card-footer">${list[i].available}</div>` +
            `<div class="orderbutton"><button onClick='DeleteSandwich(${list[i].id});' class='button button-fill button-raised button-small` +
            ` color-orange col'>Delete</button></div></div></div>`;

        }
        // de id waar card naar gestuurd zal worden.
        $$("#adminSandwichList").html(card);
      } else {
        app.dialog.alert("No snacks available!");

      }

    })
    .catch(function (error) {
      // verwerk de fout
      app.dialog.alert("fout : " + error);
    });

  return true;
}

function getListUsers() {

  let url = apiAddress + "php/producten.php?";

  opties.body = JSON.stringify({
    format: "json",
    table: "sandwiches",
    bewerking: "get"
  });

  // fetch de api
  fetch(url, opties)
    .then(function (response) {
      return response.json();
    })
    .then(function (responseData) {
      // de verwerking van de data
      var list = responseData.data;

      // er zit minstens 1 item in list
      if (list.length > 0) {
        let card = "";
        for (let i = 0, len = list.length; i < len; i++) {
          card += `<div class="card card-outline">` +
            ` <div class="card-header">${list[i].name}</div>` +
            ` <div class="card-content card-content-padding">` +
            ` <div class='block block-strong inset'>` +
            ` <p><strong>Extra:</strong> ${list[i].extra}</p>` +
            ` <p><strong>Kcal:</strong> ${list[i].kcal} </p>` +
            ` <p><strong>Saus:</strong> ${list[i].saus} </p>` +
            ` <p><strong>Prijs:</strong> € ${list[i].price}.00 </p> </div>` +
            `<div class="card-footer">${list[i].available}</div>` +
            `<div class="orderbutton"><button onClick='addOrder(${list[i].id});' class='button button-fill button-raised button-small` +
            ` color-orange col'>Order</button></div></div></div>`;
        }


        // de id waar card naar gestuurd zal worden.
        $$("#userList").html(card);
      } else {
        app.dialog.alert("No snacks available!");
      }

    })
    .catch(function (error) {
      // verwerk de fout
      app.dialog.alert("fout : " + error);
    });

  return true;
}


function DeleteSandwich(id) {

  let url = apiAddress + "php/producten.php?"

  opties.body = JSON.stringify({
    format: "json",
    table: "sandwiches",
    bewerking: "delete",
    id: id
  });
  // fetch api
  fetch(url, opties)
    .then(function (response) {
      return response.json();

    })
    // het lukt
    .then(function () {
      // de verwerking van de data
      app.dialog.alert("Item verwijderd");
      // refresh de lijst
      getListAdmin();

    })
    // het niet lukt
    .catch(function (error) {
      // verwerk de fout
      app.dialog.alert('POST failed. :' + error, "Could not delete snack!");
    });
}

// clear function
function clearAddSandwichForm() {
  $$("#name").val("")
  $$("#price").val("")
  $$("#kcal").val("")
  $$("#extra").val("")
  $$("#saus").val("")
}

function AddSandwich() {

  let url = apiAddress + "php/producten.php?"


  opties.body = JSON.stringify({
    format: "json",
    table: "sandwiches",
    bewerking: "add",
    name: $$("#name").val(),
    price: $$('#price').val(),
    available: $$("#available").val(),
    extra: $$("#extra").val(),
    kcal: $$("#kcal").val(),
    saus: $$("#saus").val(),

  });

  // validation
  if ($$("#name").hasClass("input-invalid")) {} else if ($$("#price").hasClass("input-invalid")) {} else if ($$("#extra").hasClass("input-invalid")) {} else if ($$("#kcal").hasClass("input-invalid")) {} else if ($$("#saus").hasClass("input-invalid")) {} else {
    // input succes validation


    // fetch api 
    fetch(url, opties)
      .then(function (response) {
        return response.json();
      })
      .then(function (responseData) {
        if (responseData.status === "fail") {
          app.dialog.alert("Sorry, try again.", responseData.error);
        } else {
          clearAddSandwichForm();
          app.dialog.alert("Snack added!");

        }
        // refresh de lijst
        getListAdmin();
      })
      .catch(function () {
        // verwerk de fout
        app.dialog.alert('POST failed. :' + errorThrown, "Could not add the snack, try again!");
      });
  }
}




// user login en registration


function LoginUser() {

  let url = apiAddress + "php/api.php?m=login";

  opties.body = JSON.stringify({
    email: $$("#email").val(),
    passw: $$('#password').val(),
    format: "json"
  });



  // fetch api
  fetch(url, opties)
    .then(function (response) {
      return response.json();
    })
    .then(function (responseData) {
      // test status van de response        
      if (responseData.status < 200 || responseData.status > 299) {
        // login faalde, boodschap weergeven
        app.dialog.alert('Wrong credentials', "Try again!");
        // return, zodat de rest van de fetch niet verder uitgevoerd wordt
        return;
      }
      // de verwerking van de data
      var list = responseData.data;

      // user roles toekennen
      if (list.role == 1) {
        app.views.main.router.navigate("/admin/")
      } else if (list.role == 2) {
        app.views.main.router.navigate("/home/")
      } else {
        app.views.main.router.navigate("/home/")
      }

      if (Object.keys(list).length > 0) {
        // list bevat minstens 1 property met waarde
        list.ID = parseInt(list.ID);


        app.dialog.alert("Succesfully logged in!");

      } else {
        app.dialog.alert("Could not log in!");
      }

    })
    .catch(function (error) {
      // verwerk de fout
      console.log("fout : " + error);
    });
}


function registerUser() {

  let url = apiAddress + "php/authentication.php?"
  // body data type must match "Content-Type" header
  opties.body = JSON.stringify({
    format: "json",
    table: "klanten",
    bewerking: "add",
    name: $$("#name").val(),
    family_name: $$('#family_name').val(),
    passw: $$("#password").val(),
    address: $$("#address").val(),
    email: $$("#email").val(),
    telephone_number: $$("#telephonenumber").val(),
    role: "2"
  });

  // validation
  if ($$("#name").hasClass("input-invalid")) {} else if ($$("#family_name").hasClass("input-invalid")) {} else if ($$("#password").hasClass("input-invalid")) {} else if ($$("#address").hasClass("input-invalid")) {} else if ($$("#email").hasClass("input-invalid")) {} else if ($$("#telephonenumber").hasClass("input-invalid")) {} else {
    // input succes validation


    // fetch api
    fetch(url, opties)
      .then(function (response) {
        return response.json();
      })
      .then(function (responseData) {
        if (responseData.status === "fail") {
          app.dialog.alert("Sorry, write something first");
        } else {
          app.views.main.router.navigate("/home/")
          app.dialog.alert("Welcome!", "You are now a user!");
        }

      })

      .catch(function () {
        // verwerk de fout
        app.dialog.alert("Could not register!");
      });
  }
}

// orders
function addOrder(id) {

  let url = apiAddress + "php/producten.php?";

  opties.body = JSON.stringify({
    format: "json",
    table: "Orders",
    bewerking: "addOrder",
    completed: "InProgress",
    productID: id
  });
  // fetch api
  fetch(url, opties)
    .then(function (response) {
      return response.json();
    })
    .then(function () {
      // de verwerking van de data
      app.dialog.alert("Wait for your notification", "Order succesful");
    })
    .catch(function (error) {
      // verwerk de fout
      app.dialog.alert('POST failed. :' + error, "Could not order");
    });

}

function getAllOrders() {

  let url = apiAddress + "php/authentication.php?";

  opties.body = JSON.stringify({
    format: "json",
    table: "Orders",
    bewerking: "get"
  });


  //fetch api
  fetch(url, opties)
    .then(function (response) {
      return response.json();
    })
    .then(function (responseData) {
      // de verwerking van de data
      var orderList = responseData.data;

      if (orderList.length > 0) {
        // er zit minstens 1 item in list, we geven dit ook onmiddelijk weer
        let card = "";
        for (let i = 0, len = orderList.length; i < len; i++) {
          card += `<div class="list"><ul><li>` +
            `<a href="#" class="item-link item-content">` +
            `<div class="item-inner">` +
            `<div class="item-title">` +
            ` <div class="item-header">${orderList[i].completed}</div>product id: ${orderList[i].productID}` +
            `<button onClick='afronden(${orderList[i].id});' class='button button-fill button-raised button-small color-orange col'>Finish</button></div>` +
            `</div></div></a></li>`;
        }

        $$("#OrderList").html(card);
      } else {
        app.dialog.alert("No orders available!");
      }

    })
    .catch(function (error) {
      // verwerk de fout
      app.dialog.alert("fout : " + error);
    });

  return true;
}

function afronden(id) {
 
  let url = apiAddress + "php/producten.php?";
 
  opties.body = JSON.stringify({
      format: "json",
      table: "Orders",
      bewerking: "afronden",
      id: id
  });
 
// fetch api
  fetch(url, opties)
  .then(function(response) {
      return response.json();
  })
  .then(function() {
    getAllOrders();
      // de verwerking van de data
      notificationFull.open();
  
  })
  .catch(function(error) {
      // verwerk de fout
      app.dialog.alert('POST failed. :' + error, "Could not finish order!");
  });

}


// events maps en socials
document.addEventListener("deviceready", function () {
  $$(document).on('page:init', '.page[data-name="maps"]', function (e) {
    document.getElementById("btnMaps").addEventListener('click', openMaps);

    function openMaps() {
      cordova.InAppBrowser.open('https://www.google.be/maps/', '_blank', 'location=yes');
    };
  });

  $$(document).on('page:init', '.page[data-name="socials"]', function (e) {

    document.getElementById("btnInstagram").addEventListener('click', openInstagram);
    document.getElementById("btnFB").addEventListener('click', openFacebook);
    document.getElementById("btnLink").addEventListener('click', openLinkedIn);
    document.getElementById("btnTwitter").addEventListener('click', openTwitter);

    function openInstagram() {
      cordova.InAppBrowser.open('https://www.instagram.com/', '_blank', 'location=yes');
    };

    function openFacebook() {
      cordova.InAppBrowser.open('https://www.facebook.com/', '_blank', 'location=yes');
    };

    function openLinkedIn() {
      cordova.InAppBrowser.open('https://www.linkedin.com/', '_blank', 'location=yes');
    };

    function openTwitter() {
      cordova.InAppBrowser.open('https://twitter.com/', '_blank', 'location=yes');
    };

  });


});